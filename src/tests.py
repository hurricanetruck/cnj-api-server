# coding: utf-8

import os
import tempfile

import unittest
import server


with open(os.path.join(server.ROOT_DIR, 'data', 'create_tables.sql'), 'r') as fd:
    SCHEMA_SQL = fd.read()


class BaseTest(unittest.TestCase):
    def setUp(self):
        self.db_fd, server.app.config['DATABASE'] = tempfile.mkstemp()
        server.app.config['TESTING'] = True
        self.client = server.app.test_client()

        with server.app.app_context():
            server.get_db().executescript(SCHEMA_SQL)

    def tearDown(self):
        os.close(self.db_fd)
        os.unlink(server.app.config['DATABASE'])



class TestAllJokes(BaseTest):

    def test_empty_database(self):
        response = self.client.get('/jokes')

        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.get_json(), [])

    def test_filled_database(self):
        with server.app.app_context():
            server.get_db().executemany(
                """
                INSERT INTO jokes (id, joke, rating) VALUES (?, ?, ?)
                """,
                [(1, 'Joke1', 3), (2, 'Joke2', 4)]
            )
            server.get_db().commit()

        response = self.client.get('/jokes')

        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.get_json(), [
            {'id': 1, 'joke': 'Joke1', 'rating': 3},
            {'id': 2, 'joke': 'Joke2', 'rating': 4},
        ])


class TestJokesByRatings(BaseTest):
    def setUp(self):
        super(TestJokesByRatings, self).setUp()
        with server.app.app_context():
            server.get_db().executemany(
                """
                INSERT INTO jokes (id, joke, rating) VALUES (?, ?, ?)
                """,
                [(1, 'Joke1', 3), (2, 'Joke2', 4), (3, 'Joke3', 5)]
            )
            server.get_db().commit()

    def test_jokes_not_found(self):
        response = self.client.get('/jokes?ratings=1&ratings=2')
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.get_json(), [])

    def test_jokes_found(self):
        response = self.client.get('/jokes?ratings=3&ratings=4')
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.get_json(), [
            {'id': 2, 'joke': 'Joke2', 'rating': 4},
            {'id': 1, 'joke': 'Joke1', 'rating': 3},
        ])

    def test_jokes_partially_found(self):
        response = self.client.get('/jokes?ratings=3&ratings=2')
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.get_json(), [
            {'id': 1, 'joke': 'Joke1', 'rating': 3},
        ])

    def test_invalid_ratings(self):
        response = self.client.get('/jokes?ratings=pig&ratings=cat')
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.get_json(), [])

    def test_ignore_invalid_ratings(self):
        response = self.client.get('/jokes?ratings=pig&ratings=4')
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.get_json(), [
            {'id': 2, 'joke': 'Joke2', 'rating': 4},
        ])


class TestJokesByCategories(BaseTest):
    def setUp(self):
        super(TestJokesByCategories, self).setUp()
        with server.app.app_context():
            db = server.get_db()
            db.executemany(
                """
                INSERT INTO jokes (id, joke, rating) VALUES (?, ?, ?)
                """,
                [(1, 'Joke1', 3), (2, 'Joke2', 4), (3, 'Joke3', 5)]
            )
            db.executemany(
                """
                INSERT INTO categories (id, category_name) VALUES (?, ?)
                """,
                [(1, 'nerdy'), (2, 'bad')]
            )
            db.executemany(
                """
                INSERT INTO jokes_categories (joke_id, category_id) VALUES (?, ?)
                """,
                [(1, 1), (1, 2), (2, 1)]
            )
            db.commit()

    def test_jokes_not_found(self):
        response = self.client.get('/jokes?categories=explicit')
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.get_json(), [])

    def test_jokes_found(self):
        response = self.client.get('/jokes?categories=nerdy&categories=bad')
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.get_json(), [
            {'id': 2, 'joke': 'Joke2', 'rating': 4},
            {'id': 1, 'joke': 'Joke1', 'rating': 3},
        ])

    def test_jokes_partially_found(self):
        response = self.client.get('/jokes?categories=bad')
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.get_json(), [
            {'id': 1, 'joke': 'Joke1', 'rating': 3},
        ])

    def test_ignore_invalid_ratings(self):
        response = self.client.get('/jokes?categories=bad&categories=nonexistent')
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.get_json(), [
            {'id': 1, 'joke': 'Joke1', 'rating': 3},
        ])


class TestJokesByCategoriesAndRatings(BaseTest):
    def setUp(self):
        super(TestJokesByCategoriesAndRatings, self).setUp()
        with server.app.app_context():
            db = server.get_db()
            db.executemany(
                """
                INSERT INTO jokes (id, joke, rating) VALUES (?, ?, ?)
                """,
                [(1, 'Joke1', 3), (2, 'Joke2', 4), (3, 'Joke3', 5)]
            )
            db.executemany(
                """
                INSERT INTO categories (id, category_name) VALUES (?, ?)
                """,
                [(1, 'nerdy'), (2, 'bad')]
            )
            db.executemany(
                """
                INSERT INTO jokes_categories (joke_id, category_id) VALUES (?, ?)
                """,
                [(1, 1), (1, 2), (2, 1), (3, 2)]
            )
            db.commit()

    def test_jokes_not_found(self):
        response = self.client.get('/jokes?categories=nerdy&ratings=2')
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.get_json(), [])

    def test_jokes_found(self):
        response = self.client.get('/jokes?categories=bad&ratings=5&ratings=3')
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.get_json(), [
            {'id': 3, 'joke': 'Joke3', 'rating': 5},
            {'id': 1, 'joke': 'Joke1', 'rating': 3},
        ])


class TestRandomJoke(BaseTest):

    def test_empty_database(self):
        response = self.client.get('/jokes/random')
        self.assertEqual(response.status, '501 NOT IMPLEMENTED')

    def test_get_random_joke(self):
        """
        The one-million dollar question: How to test randomness?
        Here, we'll assert that we're receiving a JSON with
        proper structure AND being sure it actually exists in the DB.
        """
        with server.app.app_context():
            server.get_db().executemany(
                """
                INSERT INTO jokes (id, joke, rating) VALUES (?, ?, ?)
                """,
                [(1, 'Joke1', 3), (2, 'Joke2', 4), (3, 'Joke3', 5)]
            )
            server.get_db().commit()

        response = self.client.get('/jokes/random')
        self.assertEqual(response.status, '200 OK')
        joke_json = response.get_json()

        self.assertIn('id', joke_json)
        self.assertIn('joke', joke_json)
        self.assertIn('rating', joke_json)

        self.assertEqual(3, len(joke_json.keys()))

        self.assertIsInstance(joke_json['id'], int)
        self.assertIsInstance(joke_json['joke'], str)
        self.assertIsInstance(joke_json['rating'], int)

        # assert we're actually returning something from DB
        with server.app.app_context():
            db = server.get_db()
            results = db.execute(
                """
                SELECT * FROM jokes
                WHERE id = ? AND joke = ? AND rating = ?
                """,
                [joke_json['id'], joke_json['joke'], joke_json['rating']]
            ).fetchall()

            self.assertEqual(1, len(results))


class TestSingleJoke(BaseTest):
    def test_not_found(self):
        response = self.client.get('/jokes/1')

        self.assertEqual(response.status, '404 NOT FOUND')

    def test_found(self):
        with server.app.app_context():
            server.get_db().executemany(
                """
                INSERT INTO jokes (id, joke, rating) VALUES (?, ?, ?)
                """,
                [(1, 'Joke1', 3), (2, 'Joke2', 4)]
            )
            server.get_db().commit()

        response = self.client.get('/jokes/1')

        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.get_json(), {'id': 1, 'joke': 'Joke1', 'rating': 3})

if __name__ == "__main__":
        unittest.main()
