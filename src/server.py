# coding: utf-8

import os
import logging
from logging.config import dictConfig
from flask import (
    abort,
    Flask,
    g,
    jsonify,
    request,
)
import sqlite3


# Logging configuration
LOGGING_CFG = dict(
    version = 1,
    formatters = {
        'f': {'format':
              '%(asctime)s %(name)-12s %(levelname)-8s %(message)s'}
        },
    handlers = {
        'h': {'class': 'logging.StreamHandler',
              'formatter': 'f',
              'level': os.getenv('LOG_LEVEL', 'DEBUG')}
        },
    root = {
        'handlers': ['h'],
        'level': os.getenv('LOG_LEVEL', 'DEBUG'),
        },
)
dictConfig(LOGGING_CFG)
logger = logging.getLogger()

# DB configuration
ROOT_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.path.pardir)
DATABASE = os.path.join(ROOT_DIR, 'data', os.getenv('DB_NAME', 'jokes.db'))
if not os.path.isfile(DATABASE):
    logger.critical('DB does not exist. (path - %s) Exiting.', DATABASE)
    exit(1)


app = Flask(__name__)
app.config.from_mapping(
    DEBUG=(os.getenv('DEBUG', False) in (True, 'on', '1')),
    DATABASE=DATABASE,
)


def get_db():
    if 'db' not in g:
        try:
            g.db = sqlite3.connect(app.config['DATABASE'])
        except Exception as e:
            logger.error('Could not connect to the database. %s', e)
            return
        g.db.row_factory = sqlite3.Row

    return g.db


ALL_JOKES_SQL = """
    SELECT j.id, j.joke, j.rating FROM jokes as j
    ORDER BY j.id
"""

JOKES_BY_RATING_SQL = """
    SELECT j.id, j.joke, j.rating FROM jokes as j
    WHERE j.rating IN (%s)
    ORDER BY j.rating DESC
"""

JOKES_BY_CATEGORIES_SQL = """
    SELECT DISTINCT j.id, j.joke, j.rating FROM jokes as j

    INNER JOIN jokes_categories as jc
    ON j.id = jc.joke_id

    INNER JOIN categories as c
    ON jc.category_id = c.id

    WHERE c.category_name IN (%s)

    ORDER BY j.rating DESC
"""

JOKES_BY_CATEGORIES_AND_RATINGS_SQL = """
    SELECT DISTINCT j.id, j.joke, j.rating FROM jokes as j

    INNER JOIN jokes_categories as jc
    ON j.id = jc.joke_id

    INNER JOIN categories as c
    ON jc.category_id = c.id

    WHERE j.rating IN (%s)
    AND c.category_name IN (%s)

    ORDER BY j.rating DESC
"""

RANDOM_JOKE_SQL = """
    SELECT j.id, j.joke, j.rating FROM jokes as j
    ORDER BY RANDOM() LIMIT 1
"""

SINGLE_JOKE_SQL = """
    SELECT j.id, j.joke, j.rating FROM jokes as j
    WHERE j.id = ?
    ORDER BY j.id
"""


@app.route("/jokes", methods=('GET',))
def jokes():

    ratings = request.args.getlist('ratings')
    categories = request.args.getlist('categories')

    if ratings and categories:
        sql = JOKES_BY_CATEGORIES_AND_RATINGS_SQL % (
            ','.join('?' * len(ratings)),
            ','.join('?' * len(categories)),
        )
        jokes = _run_query(sql, ratings + categories, 'all')
    elif categories:
        sql = JOKES_BY_CATEGORIES_SQL % ','.join('?' * len(categories))
        jokes = _run_query(sql, categories, 'all')
    elif ratings:
        sql = JOKES_BY_RATING_SQL % ','.join('?' * len(ratings))
        jokes = _run_query(sql, ratings, 'all')
    else:
        jokes = _run_query(ALL_JOKES_SQL, fetch='all')
    jokes = [_serialize(j) for j in jokes]
    return jsonify(jokes)


@app.route("/jokes/random", methods=('GET',))
def random_joke():
    joke = _run_query(RANDOM_JOKE_SQL, fetch='one')
    if not joke:
        abort(501)
    return jsonify(_serialize(joke))


@app.route("/jokes/<joke_id>", methods=('GET',))
def joke(joke_id):
    joke = _run_query(SINGLE_JOKE_SQL, (joke_id,), 'one')
    if not joke:
        abort(404)
    return jsonify(_serialize(joke))


def _serialize(joke):
    i, j, r = joke
    return {'id': i, 'joke': j, 'rating': r}


def _run_query(sql, params=None, fetch='all'):
    db = get_db()
    if not db:
        logger.error('Could not retrieve DB connection.')
        if fetch == 'all':
            return []
        return None

    if params:
        rows = db.execute(sql, params)
    else:
        rows = db.execute(sql)

    if fetch == 'all':
        return rows.fetchall()
    return rows.fetchone()


if __name__ == "__main__":
    app.run(host="0.0.0.0", port="5000")
