#! /bin/env/python3

import json
import logging
from os import (
    getenv,
    path,
)
import random
import sqlite3

DATA_DIR = path.dirname(path.abspath(__file__))

JSON_SEED = getenv('JSON_SEED', 'example.json')
JSON_SEED_PATH = path.join(DATA_DIR, JSON_SEED)

DB_NAME = getenv('DB_NAME', 'jokes.db')
DB_PATH = path.join(DATA_DIR, DB_NAME)


INSERT_JOKES_SQL = """
    INSERT OR IGNORE INTO jokes (joke, rating) VALUES (?, ?)
"""

INSERT_CATEGORIES_SQL = """
    INSERT OR IGNORE INTO categories (category_name) VALUES (?)
"""

LINK_JOKE_TO_CATEGORY_SQL = """
    INSERT INTO jokes_categories VALUES (
        (SELECT id FROM jokes WHERE joke = ?),
        (SELECT id FROM categories WHERE category_name = ?)
    )
"""

try:
    if not path.isfile(DB_PATH):
        logging.error('DB does not exist. (path - %s)', DB_PATH)
        exit(1)
    conn = sqlite3.connect(DB_PATH)
except Exception as e:
    logging.error('Could not connect to the database. %s', e)
    exit(1)

try:
    with open(JSON_SEED_PATH, 'r') as fd:
        data = json.load(fd)
except FileNotFoundError as e:
    logging.error('JSON_SEED file (path - %s) not found. Details: %s', JSON_SEED_PATH, e)
    exit(1)
except json.decoder.JSONDecodeError as e:
    logging.error('Could not parse JSON file (path - %s). Details: %s', JSON_SEED_PATH, e)
    exit(1)

jokes = []
categories = []
jokes_x_categories = []
for joke in data.get('value', []):

    joke_categories = joke.get('categories', [])
    joke_text = joke['joke']

    jokes.append((joke_text, random.randint(0,5)))

    if joke_categories:
        categories.append(joke_categories)
        jokes_x_categories.append((joke_text, joke_categories))

with conn:
    try:
        conn.executemany(INSERT_CATEGORIES_SQL, categories)
        conn.executemany(INSERT_JOKES_SQL, jokes)

        for joke, categories in jokes_x_categories:
            for category in categories:
                conn.execute(LINK_JOKE_TO_CATEGORY_SQL, (joke, category))
    except (sqlite3.OperationalError, sqlite3.DatabaseError) as e:
        logging.error('Error when executing SQL. Details: %s', e)
        exit(1)
