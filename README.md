# Chuck Norris jokes API server

Requirements
------------

 - docker
 - docker-compose


Starting from scratch
---------------------

  `make setup`

  This command will build the container, create the schema, load example data and
  run the webservice.


Running Tests
--------------

 Run `make test`


Configuration and Defaults
---------------------------

Configuration is done via the following environment variables and its defaults (see `.env` file):

- DEBUG=on
- DB_NAME=jokes.db
- JSON_SEED=example.json
- PORT=5000
- LOG_LEVEL=DEBUG

Feel free to set then when running the application or loading data, though it is
not required.


Where is my data?!
------------------

Schema creation scripts, example data, loader and
the database itself (SQLite) are stored under `./data/` directory.


What if I want more jokes?
--------------------------

Run in this order:
- `curl -X GET 'http://api.icndb.com/jokes' > ./data/icndb.json`
- `JSON_SEED=icndb.json make load_data`


Assumptions/Technical Decisions
--------------------------------

 - There are no duplicate jokes
 - The standard python logging was used, redirecting to stderr.
   Did a basic configuration in order to show where it needs to change
   if we want to write the logs to rsyslog, etc.
 - Everything is kept in a single file for simplicity.
   Though the queries and routing logic could be split into different files,
   brings little to no value. In fact, it would even complicate the codebase,
   adding a unneeded lookup.
