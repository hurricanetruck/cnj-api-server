FROM ubuntu:latest

MAINTAINER vandersonmota "vanderson.mota@gmail.com"

RUN apt-get update \
  && apt-get install -y python3-pip python3-dev sqlite3 \
  && cd /usr/local/bin \
  && ln -s /usr/bin/python3 python \
  && pip3 install --upgrade pip

ADD . /project

WORKDIR /project

RUN pip3 install -r requirements.txt

CMD ["python3", "src/server.py"]
