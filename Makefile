help:
	@echo "Available Targets:"
	@cat Makefile | egrep '^(\w+?):' | sed 's/:\(.*\)//g' | sed 's/^/- /g'

build_image:
		docker build -t chuck_norris_db .

run:
		DEBUG=1 \
		docker-compose up -d

create_schema:
		docker-compose run --rm chuck_norris_db sqlite3 ./data/jokes.db < ./data/create_tables.sql

load_data:
		docker-compose run --rm chuck_norris_db python ./data/loader.py

setup:	build_image create_schema load_data run

test:
		docker-compose run --rm chuck_norris_db python ./src/tests.py

.PHONY: build_and_run

